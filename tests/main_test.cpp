#include <integrlibild.h>
#include <string>
#include <iostream>
#include <fstream>
#include <time.h>
#ifndef WIN32
#include <unistd.h> //!< \todo Consider using async method for delays.
#else
#include <windows.h>
void sleep(time_t Seconds) { Sleep(Seconds*1000); }
#endif // WIN32
using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using ILD::Document;
using ILD::Operation;
using ILD::Copy;
using ILD::CopySet;
using std::exception;
using std::to_string;
using std::istream;
using std::ofstream;
using std::ios;

//#define TEST_ONE
// Connection parameters
const string sServiceURI = "http://base.everytag.ru";
const int iPort = 8080;
const int iVersion = 1;
// Authentication parameters
const string sUserName = "martov";
const string sPassword = "1q2w#E$R";
const string sClientId = "ild-react-ui";
const string sClientPassword = "clientpassword";
const ILD::Credentials::GrantType eGrantType = ILD::Credentials::GRANT_TYPE_PASSWORD;
const string sBadDocId = "NoSuchDocumentId";
const string sDocId = "5ca32dbed8946af4e2e9d9bc";
const string sCreatedDocId = "5cc7fe8936fec0561f4b4716";
const string sDocumentOriginal = "document.pdf";
const string sOperationId = "5cc9aecb36fec0561f4b4719";
const string sCopySetId = "5ccad30836fec0561f4b4724";
const string sCopyUser = "restlibtester";
const string sCopyId = "5ccc2a7436fec0561f4b4726";
const int iCount = 100;
	
//! Class for testing the ILD API library
class TestClient : public ILD::Client {
public:
    TestClient( const string& ServiceURI, int Port = 8080, int Version = 1 ) : Client( ServiceURI, Port, Version ) {}
protected:
    void onAuthorizationSuccess() {
	cout << "Authorization successful. ";
#ifndef TEST_ONE
	cout << "Get available documents." << endl; 
	getDocuments(); //!< \note Это - только тест работы данного вызова. Для создания документа не требуется.
#else
	cout << "Get a lot of info, asynchronously." << endl;
	for(int I = 0; I < iCount; ++I) {
	    cout << "*** Iteration " << I << " of " << iCount;
	    getDocuments();
	    getDocument(sDocId);
	    checkOperationStatus(sOperationId);
	    getSetCopies(sCopySetId);
	    getCopyContent(sCopySetId, sCopyId);
	}
#endif
    } // onAuthorizationSuccess()
    void onDocumentsReceived(const vector<Document>& Documents) {
	cout << "Received documents:" << endl;
	for( const Document& Doc : Documents ) {
	    cout << "\tid: " << Doc.id() << ", [" << Doc.title() << "] created @ " << Doc.createdTime() << " status: " << (Doc.isProtected() ? "protected" : "unprotected") << endl;
	}
#ifndef TEST_ONE
	// Get one document:
	//! \note Проверка получения документов. Для создания и защиты это также не требуется.
	cout << "Try to get document with BAD ID [" << sBadDocId << "]." << endl; 
	getDocument(sBadDocId);
	cout << "Get document with ID [" << sDocId << "]." << endl;
	getDocument(sDocId);
#endif
#ifndef TEST_ONE
	// Now create a test document
	// Стандартная цепочка:
	cout << "=> 1.Create document." << endl;
	vector<Document::Attribute> Attributes;
	Attributes.push_back(Document::Attribute("securityClass", "confidential"));
	Attributes.push_back(Document::Attribute("registrationNumber", to_string(time(0))));
	Attributes.push_back(Document::Attribute("comment", "Created by C++ API library"));
	createDocument(Document("REST API C++ lib test document "+to_string(time(0)), Attributes));
#endif
    } // onDocumentsReceived( const vector<ILD::Document>& )
    void onDocumentReceived(const Document& Doc) {
	cout << "Received document: " << Doc.id() << ", [" << Doc.title() << "] created @" << Doc.createdTime() << endl;
    } // onDocumentReceived(const Document&)
    void onDocumentCreated(const Document& Doc) {
	cout << "Created document: " << Doc.id() << ", [" << Doc.title() << "] created @" << Doc.createdTime() << endl;
	TestDocumentId = Doc.id();
	cout << "=> 2.Upload content of the document." << endl;
	uploadOriginal(Doc.id(), sDocumentOriginal);
    } // onDocumentCreated(const Document&)
    void onDocumentContentUploaded( const string& DocumentId ) {
	cout << "Document " << DocumentId << " content successfully uploaded. Ready to protect." << endl;
	cout << "=> 3.Start document protection process." << endl;
	protectDocument(DocumentId);
    } // onDocumentContentUploaded( const string& )
    void onOperationStatusReceived( const Operation& CurrentOperation ) {
	cout << "Operation state received: " + ILD::toString(CurrentOperation) << endl;
#ifndef TEST_ONE
	if(CurrentOperation.state() == "completed") {
	    if(CurrentOperation.type() == "protect") {
		cout << "=> 4.Document is protected. Create copy set." << endl;
		createCopySet("REST API library test copyset @"+to_string(time(0)));
	    }
	    else if(CurrentOperation.type() == "copy") {
		cout << "=> 6.Copy is ready. Get it's content." << endl;
		getCopyContent(CurrentOperation.copySet(), CurrentOperation.copyId());
	    }
	    return;
	}
	sleep(5); //!< Проверяем ещё раз через 5 сек.
	checkOperationStatus(CurrentOperation.id());
	// Поскольку статус операции в случае её отмены или сбоя не был указан, такой состояние не будет обнаружено и приведёт к бесконечному ожиданию.
#endif
    } // onOperationStatusReceived( const Operation& )
    void onCopySetCreated( const CopySet& NewCopySet ) {
	cout << "Created: " << ILD::toString(NewCopySet) << endl;
	cout << "=> 5.Add a copy to the set." << endl;
	addCopyToSet(NewCopySet.id(), TestDocumentId, sCopyUser);
    } // onCopySetCreated(const CopySet&)
    void onSetCopiesReceived(const string& CopySetId, const vector<Copy>& Copies) {
	cout << "Received copy set " << CopySetId << " copies: " << endl;
	for(const Copy& C : Copies) {
	    cout << "\t" << toString(C) << endl;
	}
    } // onSetCopiesReceived(const string&, const vector<Copy>&)
    void onCopyContentReceived(const string& CopySetId, const string& CopyId, const string& FileName, istream& Content) {
	cout << "=> 7.Store the copy content." << endl; 
	cout << "Received content of copy " << CopyId << " from set " << CopySetId << " to file " << FileName << endl;
	ofstream File( "copy-" + CopyId + "-" + FileName, ios::binary | ios::out );
	File << Content.rdbuf();
    } // onCopyContentReceived(const string&, const string&, const string&, istream&)
    void onAuthorizationFail(const ILD::Credentials& ClientCredentials, int HTTPResponseCode, const std::string& Response) {
	cout << "!!! Authorization failed with provided credentials. Response code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl;
    } // onAuthrizationFail(const Credentials&, int, const std::string&)
    void onRequestFail(RequestTypeCode RequestType, ObjectTypeCode ObjectType, const std::string& ObjectId, const std::string& ParentId, int HTTPResponseCode, const std::string& Response) {
	switch(RequestType) {
	case REQ_GET:
	    switch(ObjectType) {
	    case TYPE_DOCUMENT: cout << "Document with id [" << ObjectId << "] info request failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    case TYPE_OPERATION: cout << "Operation with id [" << ObjectId << "] status check failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    default:
		cout << "Can't get object of type " << ObjectType << " with id [" << ObjectId << "] info. Code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    }
	    break;
	case REQ_CREATE:
	    switch(ObjectType) {
	    case TYPE_DOCUMENT: cout << "Document creation failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    case TYPE_COPYSET: cout << "CopySet creation failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    case TYPE_COPY:
		cout << "Can't add copy of document with id [" << ObjectId << "] to copyset with id [" << ParentId << "]. Code: " << HTTPResponseCode << ", full response: [" << Response << "]"
		     << endl;
		break;
	    }
	    break;
	case REQ_UPLOAD:
	    if(ObjectType == TYPE_DOCUMENT) cout << "Document with id [" << ObjectId << "] upload failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl;
	    else cout << "Upload of wrong object type " << ObjectType << " failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl;
	    break;
	case REQ_PROTECT:
	    if(ObjectType == TYPE_DOCUMENT) cout << "Document with id [" << ObjectId << "] protection failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl;
	    else cout << "Protection of wrong object type " << ObjectType << " failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl;
	    break;
	case REQ_GET_CONTENT:
	    switch(ObjectType) {
	    case TYPE_FOLDER: cout << "Get documents failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    case TYPE_DOCUMENT: cout << "Get content (copy) of document id[" << ObjectId << "] failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    case TYPE_COPYSET: cout << "Get content (copies list) of copyset id[" << ObjectId << "] failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    case TYPE_COPY:
		cout << "Get content of copy id[" << ObjectId << "] from set id[" << ParentId << "] failed with code: " << HTTPResponseCode << ", full response: [" << Response << "]" << endl; break;
	    default:
		cout << "Can't get content of object of type " << ObjectType << " with id [" << ObjectId << "], parent id[" << ParentId << "]. Code: " << HTTPResponseCode
							    << ", full response: [" << Response << "]" << endl;
		break;
	    }
	    break;
	default:
	    cout << "Request of unknown " << RequestType << " on object of type " << ObjectType << " with id [" << ObjectId << "], parent id[" << ParentId << "]. Code: " << HTTPResponseCode
		 << ", full response: [" << Response << "]" << endl;
	    break;
	}
    } // onRequestFail(RequestTypeCode, ObjectTypeCode, const std::string&, const std::string&, int, const std::string&)
    string TestDocumentId;
}; // TestClient

int main() {
    // Create connection to the ILD host
    TestClient Ild( sServiceURI, iPort, iVersion );
    // Authorize (get OAuth2 token)
    //! \todo Test authorization fail & success.
    try { //! \note You don't have to set anything but user name & password.
	cout << "=> 0.Start with authorization." << endl;
	Ild.authorize(ILD::Credentials(sUserName, sPassword).setGrantType(eGrantType).setClientName(sClientId).setClientPassword(sClientPassword));
	cout << "=> W.Check for the pending requests." << endl;
	while(Ild.checkRequests()) sleep(7);
	cout << "=> X.Wait for the rest of requests." << endl;
	Ild.waitRequests();
	cout << "=> Y.The work complete." << endl;
    } catch(exception& E) {
	cerr << "Something gone wrong: " << E.what() << endl;
    }
    cout << "=> Z.End finally return." << endl;
    return 0;
} // main()
