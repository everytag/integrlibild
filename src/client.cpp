#include "integrlibild.h"
#include <mutex>
#include <cpprest/http_client.h>
#include <pplx/pplxtasks.h>
#include <cpprest/json.h>
using std::vector;
using std::map;
using std::pair;
using std::mutex;
using std::lock_guard;
using std::string;
using std::to_string;
using web::http::methods;
using web::http::http_request;
using web::http::http_response;
using web::http::http_headers;
using web::uri_builder;
using pplx::task;
using std::cout;
using std::cerr;
using std::endl;
using utility::conversions::to_base64;
using utility::conversions::to_utf8string;
using utility::conversions::to_string_t;
using utility::string_t;

#include <fstream>
using std::ifstream;
using std::ios;
using std::istream;
#include <sstream>
using std::stringstream;
#include <cpprest/filestream.h>
using concurrency::streams::file_buffer;
using concurrency::streams::streambuf;
using concurrency::streams::container_buffer;
#include <boost/filesystem/path.hpp>
using boost::filesystem::path;
#include <boost/algorithm/string.hpp>
using boost::algorithm::to_lower;
//#include <algorithm>
using std::find;
using std::list;
using std::exception;
//#define DEBUG_PRINT

namespace ILD {
    // Helper functions. Follow C++ REST SDK syntax conventions.
    string Client::http_code_description(int Code) {
	switch(Code) {
	case 100: return "Continue";
	case 101: return "Switching Protocol";
	case 200: return "OK";
	case 201: return "Created";
	case 202: return "Accepted";
	case 203: return "Non-Authoritative Information";
	case 204: return "No Content";
	case 205: return "Reset Content";
	case 206: return "Partial Content";
	case 300: return "Multiple Choices";
	case 301: return "Moved Permanently";
	case 302: return "Found";
	case 303: return "See Other";
	case 304: return "Not Modified";
	case 307: return "Temporary Redirect";
	case 308: return "Permanent Redirect";
	case 400: return "Bad Request";
	case 401: return "Unauthorized";
	case 403: return "Forbidden";
	case 404: return "Not Found";
	case 405: return "Method Not Allowed";
	case 406: return "Not Acceptable";
	case 407: return "Proxy Authentication Required";
	case 408: return "Request Timeout";
	case 409: return "Conflict";
	case 410: return "Gone";
	case 411: return "Length Required";
	case 412: return "Precondition Failed";
	case 413: return "Payload Too Large";
	case 414: return "URI Too Long";
	case 415: return "Unsupported Media Type";
	case 416: return "Range Not Satisfiable";
	case 417: return "Expectation Failed";
	case 418: return "I'm a teapot";
	case 422: return "Unprocessable Entity";
	case 425: return "Too Early";
	case 426: return "Upgrade Required";
	case 428: return "Precondition Required";
	case 429: return "Too Many Requests";
	case 431: return "Request Header Fields Too Large";
	case 451: return "Unavailable For Legal Reasons";
	case 500: return "Internal Server Error";
	case 501: return "Not Implemented";
	case 502: return "Bad Gateway";
	case 503: return "Service Unavailable";
	case 504: return "Gateway Timeout";
	case 505: return "HTTP Version Not Supported";
	case 511: return "Network Authentication Required";
	}
	return "Unknown HTTP code";
    } // http_code_description(int)
    string get_mime_type(const string& FileName) {
	string Ext = path(FileName).extension().generic_string();
	to_lower(Ext);
	if(Ext == ".pdf") return "application/pdf";
	if(Ext == ".png") return "image/png";
	if(Ext == ".jpg" || Ext == ".jpeg") return "image/jpeg";
	if(Ext == ".bmp") return "image/bmp";
	if(Ext == ".gif") return "image/gif";
	if(Ext == ".tif" || Ext == ".tiff") return "image/tiff";
	return "application/octet-stream";
    } // get_mime_type(const string&)
    vector<unsigned char> string_to_vector(const string& Str) {
	vector<unsigned char> V;
	for(char Ch : Str) V.push_back(Ch);
	return V;
    } // string_to_vector(const string&)
    timems_t parse_time(const std::string& TimeString) { //!< Переводит строку в формате YYYY-MM-DDTHH:mm:SS.sss с зоной в формате +/-HHmm в число миллисекунд
	int ZonePos = 19;
	if(TimeString.size() <= ZonePos) return 0;
	int Year = atoi(TimeString.substr(0,4).c_str());
	int Month = atoi(TimeString.substr(5,2).c_str());
	int Day = atoi(TimeString.substr(8,2).c_str());
	int Hour = atoi(TimeString.substr(11,2).c_str());
	int Minute = atoi(TimeString.substr(14,2).c_str());
	int Second = atoi(TimeString.substr(17,2).c_str());
	int Milliseconds = 0;
	if(TimeString[ZonePos] == '.') {
	    Milliseconds = atoi(TimeString.substr(ZonePos+1,3).c_str());
	    ZonePos+=4;
	}
#if 0 //! RFFU \todo Find a better way for crossplatrof parsing date string with time zone.
	char ZoneSign = TimeString[ZonePos];
	int ZoneHours = atoi(TimeString.substr(ZonePos+1,2).c_str());
	int ZoneMinutes = atoi(TimeString.substr(ZonePos+3,2).c_str());
#endif
	std::tm tmTime;
	tmTime.tm_year = Year-1900;
	tmTime.tm_mon = Month-1;
	tmTime.tm_mday = Day;
	tmTime.tm_hour = Hour;
	tmTime.tm_min = Minute;
	tmTime.tm_sec = Second;
	tmTime.tm_isdst = -1;
	timems_t Result = mktime(&tmTime);
	return Result*1000+Milliseconds;
    } // parse_time(const std::string& TimeString)
    std::string formatTime(timems_t Time) { return to_string(Time); }
    
    string get_field(const web::json::value& Val, const string_t& Key, const string& Default = string()) {
	if(Val.has_field(Key)) {
	    if(Val.at(Key).is_null()) return string();
	    else return to_utf8string(Val.at(Key).as_string());
	}
	return Default;
    } // get_field(const web::json::value&, const string&, const string&)
    double get_double_field(const web::json::value& Val, const string_t& Key, double Default = 0) {
	if(Val.has_field(Key)) {
	    if(Val.at(Key).is_null()) return Default;
	    else return Val.at(Key).as_double();
	}
	return Default;
    } // get_doble_field(const web::json::value&, const string&, double)
    int get_int_field(const web::json::value& Val, const string_t& Key, int Default = 0) {
	if(Val.has_field(Key)) {
	    if(Val.at(Key).is_null()) return Default;
	    else return Val.at(Key).as_integer();
	}
	return Default;
    } // get_int_field(const web::json::value&, const string&, int)
    
    string find_content_file_name(http_headers& Headers) {
	if(Headers.has(U("Content-Disposition"))) {
	    string Disp = to_utf8string(Headers[U("Content-Disposition")]);
#ifdef DEBUG_PRINT
	    cout << "Disposition: [" << Disp << "]" << endl;
#endif
	    int Pos = Disp.find("filename=");
	    if(Pos != string::npos) {
		return Disp.substr(Pos+9);
	    }
	}
	return "";
    } // find_content_file_name(http_headers&)
    
    Operation operationFromJson(const web::json::value& Val) {
	string sId = get_field(Val, U("id"));
	string sDocumentId = get_field(Val, U("document"));
	string sUserName = get_field(Val, U("userName"));
	double dProgress = get_double_field(Val, U("progress"));
	timems_t msCreateDate = parse_time(get_field(Val, U("createDate"), "1970-01-01T00:00:00.000+0000"));
	timems_t msStartDate = parse_time(get_field(Val, U("startDate"), "1970-01-01T00:00:00.000+0000"));
	timems_t msEndDate = parse_time(get_field(Val, U("endDate"), "1970-01-01T00:00:00.000+0000"));
	string sState = get_field(Val, U("state"));
	string sType = get_field(Val, U("type"));
	string sEstimate = get_field(Val, U("estimate"));
	string sForUser = get_field(Val, U("forUser"));
	string sCopySet = get_field(Val, U("copySet"));
	string sCopyId = get_field(Val, U("copyId"));
	string sError = get_field(Val, U("error"));
	string sAccessType = get_field(Val, U("accessType"));
	return Operation(sId, sType, sDocumentId, sUserName, sState, dProgress, msCreateDate, msStartDate, msEndDate, sEstimate, sForUser, sCopySet, sCopyId, sError, sAccessType);
    } // operationFromJson(const web::json::value& )
    Operation::Operation(const std::string& Id, const std::string& Type, const std::string& DocumentId, const std::string& UserName, const std::string& State, double Progress,
			 timems_t CreateDate, timems_t StartDate, timems_t EndDate, const std::string& Estimate, const std::string& ForUser, const std::string& CopySet, const std::string& CopyId,
			 const std::string& Error, const std::string& AccessType)
	: m_sId(Id), m_sDocumentId(DocumentId), m_sUserName(UserName), m_dProgress(Progress), m_msCreateDate(CreateDate), m_msStartDate(StartDate), m_msEndDate(EndDate), m_sState(State),
	  m_sType(Type), m_sEstimate(Estimate), m_sForUser(ForUser), m_sCopySet(CopySet), m_sCopyId(CopyId), m_sError(Error), m_sAccessType (AccessType) {
    }
    std::string toString(const Operation& Op) {
    	return "Operation: id=" + Op.id() + ", documentId=" + Op.documentId() + ", userName=\"" + Op.userName() + "\", progress=" + to_string(Op.progress())
	    + ", createDate=" + formatTime(Op.createDate()) + ", startDate=" + formatTime(Op.startDate()) + ", endDate=" + formatTime(Op.endDate()) + ", state=" + Op.state() + ", type=" + Op.type()
	    + ", estimate=" + Op.estimate() + ", forUser=" + Op.forUser() + ", copySet=" + Op.copySet() + ", copyId=" + Op.copyId() + ", error=" + Op.error() + ", accessType=" + Op.accessType();
    } // toString(const Operation&)

    Copy copyFromJson(const web::json::value& Val) {
	string sId = get_field(Val, U("id"));
	string sCopySetId = get_field(Val, U("copySet"));
	string sDocumentId = get_field(Val, U("document"));
	string sFileId = get_field(Val, U("fileId"));
	string sUserName = get_field(Val, U("userName"));
	string sForUser = get_field(Val, U("forUser"));
	timems_t msCreateDate = parse_time(get_field(Val, U("createDate"), "1970-01-01T00:00:00.000+0000"));
	timems_t msDeleteDate = parse_time(get_field(Val, U("deleteDate"), "1970-01-01T00:00:00.000+0000"));
	string sAccess = get_field(Val, U("access"));
	vector<Copy::Map> vMaps;
	if(Val.has_field(U("maps")))
	    for(const web::json::value& MapVal : Val.at(U("maps")).as_array()) {
		int iNumber = get_int_field(MapVal, U("number"));
		map<int,Copy::Operation> mOps;
		if(MapVal.has_field(U("operations"))) {
		    const web::json::object& joOps = MapVal.at(U("operations")).as_object();
		    for(auto It = joOps.begin(); It != joOps.end(); ++It ) {
			mOps[atoi(to_utf8string(It->first).c_str())] = Copy::Operation(get_int_field(It->second, U("dx")), get_int_field(It->second, U("dy")));
		    }
		}
		vMaps.push_back(Copy::Map(iNumber, mOps));
	    }
	return Copy(sId, sCopySetId, sDocumentId, sFileId, sUserName, sForUser, msCreateDate, msDeleteDate, sAccess, vMaps);
    } // copyFromJson(const web::json::value&)
    Copy::Operation::Operation(int DX, int DY) : m_iDX(DX), m_iDY(DY) {}
    Copy::Map::Map(int Number, const map<int,Operation>& Operations) : m_iNumber(Number), m_mOperations(Operations) {}
    void Copy::Map::setOperation(int Number, const Operation& NewOperation) { m_mOperations[Number] = NewOperation; }
    Copy::Copy(const std::string& Id, const std::string& CopySetId, const std::string& DocumentId, const std::string& FileId, const std::string& UserName, const std::string& ForUser, 
	       timems_t CreateDate, timems_t DeleteDate, std::string Access, const std::vector<Map>& Maps)
	: m_sId(Id), m_sCopySetId(CopySetId), m_sDocumentId(DocumentId), m_sFileId(FileId), m_sUserName(UserName), m_sForUser(ForUser), m_msCreateDate(CreateDate), m_msDeleteDate(DeleteDate),
	  m_sAccess(Access), m_vMaps(Maps) {}
    string toString(const Copy& TheCopy) {
	string Result = "Copy: id=" + TheCopy.id() + ", copySet=" + TheCopy.copySetId() + ", document=" + TheCopy.documentId() + ", fileId=" + TheCopy.fileId()
	    + ", createDate=" + formatTime(TheCopy.createDate()) + ", deleteDate=" + formatTime(TheCopy.deleteDate()) + ", userName=" + TheCopy.userName() + ", forUser=" + TheCopy.forUser()
	    + ", access=" + TheCopy.access() + ", maps=[";
	for(const Copy::Map& M : TheCopy.maps()) {
	    Result += "{ number=" + to_string(M.number()) + ", operations("+to_string(M.operations().size())+")=[";
	    for(const pair<int,Copy::Operation>& Op : M.operations()) {
		Result += "{" + to_string(Op.first) + ": {dx=" + to_string(Op.second.dx()) + ", dy=" + to_string(Op.second.dy()) + "}} ";
	    }
	    Result += "]}";
	}
	return Result + "]";
    } // toString(const Copy&)

    CopySet copySetFromJson(const web::json::value& Val) {
	string sId = get_field(Val, U("id"));
	string sTitle = get_field(Val, U("title"));
	timems_t msCreateDate = parse_time(get_field(Val, U("createDate"), "1970-01-01T00:00:00.000+0000"));
	timems_t msHandedDate = parse_time(get_field(Val, U("handedDate"), "1970-01-01T00:00:00.000+0000"));
	return CopySet(sId, sTitle, msCreateDate, msHandedDate);
    } // copySetFromJson(const web::json::value& )
    CopySet::CopySet(const string& Id, const string& Title, timems_t CreateDate, timems_t HandedDate) : m_sId(Id), m_sTitle(Title), m_msCreateDate(CreateDate), m_msHandedDate(HandedDate) {}
    string toString(const CopySet& Set) { //! \todo Format time to human-readable form.
	return "Copy set: id=" + Set.id() + ", title=\"" + Set.title() + "\", createDate=" + to_string(Set.createDate()) + ", handedDate=" + to_string(Set.handedDate());
    } // to_string(const CopySet&)
    
    Document::Attribute::Attribute(const std::string& Name, const std::string& Value, const std::string& Type) : m_sName(Name), m_sType(Type), m_sValue(Value) {}
    
    Document documentFromJson(const web::json::value& DocVal) {
	//! \todo Move to the static function (json::value -> Document)
	string sId = get_field(DocVal, U("id"));
	string sTitle = get_field(DocVal, U("title"));
#ifdef DEBUG_PRINT
	cout << "process document " << sTitle << " with id " << sId << endl;
#endif
	string sCreatedBy = get_field(DocVal, U("createdBy"));
	string sModifiedBy = get_field(DocVal, U("modifiedBy"));
	timems_t timeCreated = parse_time(get_field(DocVal, U("dateCreated"), "1970-01-01T00:00:00.000+0000"));
	timems_t timeModified = parse_time(get_field(DocVal, U("dateModified"), "1970-01-01T00:00:00.000+0000"));
	string sContentId = get_field(DocVal, U("contentId"));
	Document::StateCode eState = (get_field(DocVal, U("state"), "unprotected") == "protected") ? Document::STATE_PROTECTED : Document::STATE_UNPROTECTED;
	vector<Document::Attribute> vAttributes;
	if(DocVal.has_field(U("attributes")))
	    for(web::json::value AttrVal : DocVal.at(U("attributes")).as_array())
		vAttributes.push_back(Document::Attribute(get_field(AttrVal, U("name"), "noname"), get_field(AttrVal, U("value")), get_field(AttrVal, U("type"), "string")));
	return Document(sId, sTitle, sCreatedBy, sModifiedBy, timeCreated, timeModified, sContentId, eState, vAttributes);
    } // documentFromJson(const web::json::value& )
    
    Document::Document(const std::string& Title, const std::vector<Attribute>& Attributes)
	: m_sTitle(Title), m_timeCreated(0), m_timeModified(0), m_eState(STATE_UNPROTECTED),  m_vAttributes(Attributes) {}
    Document::Document(const std::string& Id, const std::string& Title, const std::string& CreatedBy, const std::string& ModifiedBy, timems_t TimeCreated, timems_t TimeModified,
		       const std::string& ContentId, StateCode State, const std::vector<Attribute>& Attributes)
	: m_sId(Id), m_sTitle(Title), m_sCreatedBy(CreatedBy), m_sModifiedBy(ModifiedBy), m_sContentId(ContentId), m_timeCreated(TimeCreated), m_timeModified(TimeModified), m_eState(State),
	  m_vAttributes(Attributes) {}

    Credentials::Credentials(const std::string& UserName, const std::string& UserPassword)
	: m_sUserName(UserName), m_sUserPassword(UserPassword), m_sClientName("ild-react-ui"), m_sClientPassword("clientpassword"), m_eGrantType(GRANT_TYPE_PASSWORD) {}
    
    class Client::ClientPrivate {
    public:
	class RequestInfo {
	public:
	    RequestInfo(const task<void>& Task, int HTTPResponseCode = -1, const string& Response = string() ) : m_Task(Task), m_iHTTPResponseCode( HTTPResponseCode ), m_sResponse( Response ) {}
	    virtual ~RequestInfo();
	    virtual void failNotify(Client* Master) = 0;
	    bool isDone() const { return m_Task.is_done(); }
	    void get(Client* Master) {
		try{ m_Task.get(); } catch(const exception& Ex) {
		    m_sResponse = Ex.what();
		    failNotify(Master);
		}
	    } // get()
	    void wait(Client* Master) {
		try{ m_Task.wait(); } catch(const exception& Ex) {
		    m_sResponse = Ex.what();
		    failNotify(Master);
		}
	    } // what()
	    bool operator==(const task<void>& OtherTask) const { return m_Task == OtherTask; }
	    const task<void>& getTask() const { return m_Task; }
	protected:
	    task<void> m_Task;
	    int m_iHTTPResponseCode;
	    string m_sResponse;
	}; // RequestInfo
	class AuthorizationRequestInfo : public RequestInfo {
	public:
	    AuthorizationRequestInfo(const task<void>& Task, const Credentials& Cred, int HTTPResponseCode = -1, const string& Response = string())
		: RequestInfo( Task, HTTPResponseCode, Response ), m_Credentials(Cred) {}
	    void failNotify(Client* Master);
	protected:
	    Credentials m_Credentials;
	}; // AuthorizationRequestInfo
	class GenericRequestInfo : public RequestInfo {
	public:
	    GenericRequestInfo(const task<void>& Task, RequestTypeCode RequestType = REQ_UNKNOWN, ObjectTypeCode ObjectType = TYPE_UNKNOWN, const std::string& ObjectID = string(),
			       const string& ParentID = string(), int HTTPResponseCode = -1, const string& Response = string())
		: RequestInfo(Task, HTTPResponseCode, Response), m_eRequestType(RequestType), m_eObjectType(ObjectType), m_sObjectID(ObjectID), m_sParentID(ParentID) {}
	    void failNotify(Client* Master);
	private:
	    RequestTypeCode m_eRequestType;
	    ObjectTypeCode m_eObjectType;
	    string m_sObjectID;
	    string m_sParentID;
	}; // GenericRequestInfo
	ClientPrivate(const string& ServiceURI, int Port) : m_Http(uri_builder(to_string_t(ServiceURI)).set_port(Port).to_uri()) {}
	~ClientPrivate() { waitTasks(); }
	web::http::client::http_client& http() { return m_Http; }
	mutex& accessMutex() { return m_AccessMutex; }
	const task<void>& addTask(RequestInfo* Task);
	void removeTask(RequestInfo* Task);
	size_t checkTasks(Client* Master = nullptr);
	void waitTasks(Client* Master = nullptr);
    private:
	mutex m_AccessMutex;
	web::http::client::http_client m_Http;
	list<RequestInfo*> m_vPendingTasks;
    }; // ClientPrivate
    Client::ClientPrivate::RequestInfo::~RequestInfo() {}
    void Client::ClientPrivate::AuthorizationRequestInfo::failNotify(Client* Master) { if(Master) Master->onAuthorizationFail(m_Credentials, m_iHTTPResponseCode, m_sResponse);
	else cout << "Null master on auth fail." << endl; }
    void Client::ClientPrivate::GenericRequestInfo::failNotify(Client* Master) {
	if(Master) Master->onRequestFail(m_eRequestType, m_eObjectType, m_sObjectID, m_sParentID, m_iHTTPResponseCode, m_sResponse);	
	else cout << "Null master on generic fail." << endl; 
    } // failNotify(Client*)

    const task<void>& Client::ClientPrivate::addTask(RequestInfo* Task) {
	{
	    lock_guard<mutex> Lock(m_AccessMutex);
	    if(find(m_vPendingTasks.begin(), m_vPendingTasks.end(), Task) == m_vPendingTasks.end()) {
		m_vPendingTasks.push_back(Task);
		Task->getTask().then([this,Task](){ removeTask(Task); });
	    }
	}
	return Task->getTask();
    } // addTask(RequestInfo*)
    void Client::ClientPrivate::removeTask(RequestInfo* Task) {
	lock_guard<mutex> Lock(m_AccessMutex);
#ifdef DEBUG_PRINT
	cout << "Remove task from " << m_vPendingTasks.size() << endl;
#endif
	auto It = find(m_vPendingTasks.begin(), m_vPendingTasks.end(), Task);
	if(It != m_vPendingTasks.end()) {
	    delete *It;
	    m_vPendingTasks.erase(It);
	}
#ifdef DEBUG_PRINT
	else
	    cerr << "Warning: Task not found between " << m_vPendingTasks.size() << " others." << endl;
#endif
    } // removeTask(RequestInfo*)
    size_t Client::ClientPrivate::checkTasks(Client* Master) {
	m_AccessMutex.lock();
#ifdef DEBUG_PRINT
	cout << "Checking " << m_vPendingTasks.size() << " tasks in the queue." << endl;
#endif
	//! \todo There should be no exceptions here, but enclose in try/catch anyway
	list<RequestInfo*> DoneTasks;
	for(auto It = m_vPendingTasks.begin(); It != m_vPendingTasks.end(); ++It)
	    if((*It)->isDone()) {
		DoneTasks.push_back(*It);
		m_vPendingTasks.erase(It--);
	    }
	size_t Count = m_vPendingTasks.size();
	m_AccessMutex.unlock();
	for(RequestInfo* T : DoneTasks) {
	    T->get(Master);
	    delete T;
	}
	return Count;
    } // checkTasks(Client* Master)
    void Client::ClientPrivate::waitTasks(Client* Master) {
	m_AccessMutex.lock();
#ifdef DEBUG_PRINT
	cout << "Waiting for " << m_vPendingTasks.size() << " tasks to complete." << endl;
#endif
	while(!m_vPendingTasks.empty()) {
	    RequestInfo* T = m_vPendingTasks.back();
	    m_AccessMutex.unlock();
	    T->wait(Master);
	    removeTask(T); // There should be no this task already if it is not failed.
	    m_AccessMutex.lock();
	}
	m_AccessMutex.unlock();
    } // waitTasks(Client* Master)

    Client::Client(const std::string& ServiceURI, int Port, int Version)
	: m_sServiceURI(ServiceURI), m_sAPIPath("/api/v"+to_string(Version)), m_iPort(Port), m_iVersion(Version), m_pPrivate( new ClientPrivate(ServiceURI, Port) )
    {}
    Client::~Client() {
	waitRequests();
	delete m_pPrivate;
    } // ~Client() BTW in this case tasks list must be empty.
    size_t Client::checkRequests() { return m_pPrivate->checkTasks(this); }
    void Client::waitRequests() { m_pPrivate->waitTasks(this); }
    void Client::authorize( const Credentials& Cred ) {
	http_request Req(methods::POST);
	Req.set_request_uri(U("/oauth/token"));
	Req.headers().set_content_type(U("application/x-www-form-urlencoded"));
	Req.headers().add(U("Authorization"), U("Basic ") + to_base64(string_to_vector(Cred.clientName()+":"+Cred.clientPassword())));
	string_t sGrantType = U("unknown");
	if(Cred.grantType() == Credentials::GRANT_TYPE_PASSWORD) sGrantType = U("password");
	Req.set_body( uri_builder().append_query(U("client_id"), to_string_t(Cred.clientName()),true)
		      .append_query(U("username"), to_string_t(Cred.userName()), true)
		      .append_query(U("password"), to_string_t(Cred.userPassword()), true)
		      .append_query(U("grant_type"), sGrantType, true).query() );
	m_sUserName = Cred.userName();
#ifdef DEBUG_PRINT
	cout << "Sending request: " << to_utf8string(Req.to_string()) << endl;
#endif
	try {
	    m_pPrivate->addTask(new ClientPrivate::AuthorizationRequestInfo(m_pPrivate->http().request(Req).then([this,Cred](http_response Resp) {
		if(Resp.status_code() != 200) {
		    onAuthorizationFail(Cred, Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::AuthorizationRequestInfo(Resp.extract_json().then([this,Cred,Resp](web::json::value Val) {
#ifdef DEBUG_PRINT
		    cout << "Response data:" << to_utf8string(Val.to_string()) << endl;
#endif
		    if(Val.has_field(U("access_token"))) {
			{
			    lock_guard<mutex> Lock(m_pPrivate->accessMutex());
			    m_sOAuth2Token = to_utf8string(Val[U("access_token")].as_string());
			}
			onAuthorizationSuccess();
		    }
		    else {
			cerr << "No token in the reply." << endl;
			onAuthorizationFail(Cred, Resp.status_code(), to_utf8string(Resp.to_string()));
		    }
													 }), Cred));
														 }), Cred));
	}
	catch( const exception& Ex ) { onAuthorizationFail(Cred, -1, Ex.what()); }
    } // authorize( const std::string&, const std::string&, const std::string&, GrantType )
    bool Client::isAuthorized() { std::lock_guard<std::mutex> Lock(m_pPrivate->accessMutex()); return !m_sOAuth2Token.empty(); }
    void Client::unAuthorize() { std::lock_guard<std::mutex> Lock(m_pPrivate->accessMutex()); m_sOAuth2Token.clear(); }
    void Client::onAuthorizationSuccess() {}
    void Client::onAuthorizationFail(const Credentials& ClientCredentials, int HTTPResponseCode, const std::string& Response) {}

    void Client::getDocuments() {
	http_request Req(methods::GET);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/documents"));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Get documents: " << to_utf8string(Resp.to_string()) << endl;
#endif
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_GET_CONTENT, TYPE_FOLDER, string(), string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this](web::json::value Val) {
#ifdef DEBUG_PRINT
		    cout << "Received docs (JSON): " << to_utf8string(Val.to_string()) << endl;
#endif
		    if(Val.has_field(U("documents"))) {
			std::vector<Document> vDocs;
			for(web::json::value DocVal : Val[U("documents")].as_array())
			    if(!DocVal.has_field(U("type")) || DocVal[U("type")].as_string() == U("document"))
				vDocs.push_back(documentFromJson(DocVal));
			onDocumentsReceived(vDocs);
		    } //! \todo else throw exception: no "documents" field in the response
		}), REQ_GET_CONTENT, TYPE_FOLDER));
	    }), REQ_GET_CONTENT, TYPE_FOLDER));
	}
	catch(const exception& Ex) { onRequestFail(REQ_GET_CONTENT, TYPE_FOLDER, string(), string(), -1, Ex.what()); }
    } // getDocuments()
    void Client::onDocumentsReceived( const vector<Document>& vDocuments ) {}

    void Client::createDocument(const Document& NewDocument) {
	http_request Req(methods::POST);
	Req.set_request_uri(uri_builder().set_path(to_string_t(m_sAPIPath+"/documents")).append_query(U("user"),to_string_t(m_sUserName)).to_uri());
	Req.headers().set_content_type(U("application/json;charset=UTF-8"));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	web::json::value DocVal;
	DocVal[U("type")] = web::json::value(U("document"));
	DocVal[U("title")] = web::json::value(to_string_t(NewDocument.title()));
	if(NewDocument.attributes().size() > 0) {
	    web::json::value AttrVal;
	    for(const Document::Attribute& Attr : NewDocument.attributes()) {
		web::json::value NewAttr;
		NewAttr[U("name")] = web::json::value(to_string_t(Attr.name()));
		NewAttr[U("type")] = web::json::value(to_string_t(Attr.type()));
		NewAttr[U("value")] = web::json::value(to_string_t(Attr.value()));
		AttrVal[AttrVal.size()] = NewAttr;
	    }
	    DocVal[U("attributes")] = AttrVal;
	}
	Req.set_body(DocVal);
#ifdef DEBUG_PRINT
	cout << to_utf8string(Req.to_string()) << endl;
#endif
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Create document response: " << to_utf8string(Resp.to_string()) << endl;
#endif
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_CREATE, TYPE_DOCUMENT, string(), string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this](web::json::value Val) {
#ifdef DEBUG_PRINT
			cout << "New document (JSON): " << to_utf8string(Val.to_string()) << endl;
#endif
			onDocumentCreated(documentFromJson(Val));
		}), REQ_CREATE, TYPE_DOCUMENT));
	    }), REQ_CREATE, TYPE_DOCUMENT));
	}
	catch(const exception& Ex) { onRequestFail(REQ_CREATE, TYPE_DOCUMENT, string(), string(), -1, Ex.what()); }
    } // createDocument(const Document&)
    void Client::onDocumentCreated( const Document& NewDocument ) {}
    
    void Client::uploadOriginal( const string& DocumentId, const string& FileName, istream& Content) {
	http_request Req(methods::POST);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/documents/"+DocumentId+"/original"));
	string sBoundary = "58a1debe00864a7816293b55784d65b8b48be17b"; //!< \todo Use really random boundary
	Req.headers().set_content_type(to_string_t("multipart/form-data; boundary="+sBoundary));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	stringstream sContent; //!< \todo URGENT! Use asynchronous stream for this all.
	//! \todo Determine content type by file extension.
	sContent << "--" << sBoundary << "\r\nContent-Disposition: form-data; name=\"document\"; filename=\"" << FileName << "\"\r\nContent-Type: " << get_mime_type(FileName) << "\r\n\r\n";
	sContent << Content.rdbuf();
	sContent << "\r\n--" << sBoundary << "--\r\n";
	Req.set_body(sContent.str()); //! \todo Добавить возможность отслеживать процесс загрузки документа.
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this,DocumentId](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Upload original: " << to_utf8string(Resp.to_string()) << endl;
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_string().then([](const string_t& Str) { cout << "Response: " << to_utf8string(Str) << endl; })));
#endif	
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_UPLOAD, TYPE_DOCUMENT, DocumentId, string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		onDocumentContentUploaded(DocumentId);
	    }), REQ_UPLOAD, TYPE_DOCUMENT, DocumentId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_UPLOAD, TYPE_DOCUMENT, DocumentId, string(), -1, Ex.what()); }
    } // uploadOriginal(const std::string&, const std::string&)
    void Client::uploadOriginal( const string& DocumentId, const string& FileName) {
	ifstream File(FileName, ios::binary);
	if(File.good()) uploadOriginal(DocumentId, FileName, File);
	else onRequestFail(REQ_UPLOAD, TYPE_DOCUMENT, DocumentId, string(), -2, "Can't open input file [" + FileName + "]." ); //!< \todo Add more error info.
    } // uploadOriginal(const string&, const string&)
    void Client::onDocumentContentUploaded( const string& DocumentId ) {}

    void Client::protectDocument( const string& DocumentId ) {
	http_request Req(methods::POST);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/operations"));
	Req.headers().set_content_type(U("application/json;charset=UTF-8"));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	web::json::value Val;
	Val[U("type")] = web::json::value(U("protect"));
	Val[U("document")] = web::json::value(to_string_t(DocumentId));
	Req.set_body(Val);
#ifdef DEBUG_PRINT
	cout << to_utf8string(Req.to_string()) << endl;
#endif
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this,DocumentId](http_response Resp) {
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_PROTECT, TYPE_DOCUMENT, DocumentId, string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this](web::json::value Val) {
#ifdef DEBUG_PRINT
		    cout << "Protect document operation started: " << to_utf8string(Val.to_string()) << endl;
#endif
		    onOperationStatusReceived(operationFromJson(Val));
		}), REQ_PROTECT, TYPE_DOCUMENT, DocumentId));
	    }), REQ_PROTECT, TYPE_DOCUMENT, DocumentId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_PROTECT, TYPE_DOCUMENT, DocumentId, string(), -1, Ex.what()); }
    } // protectDocument( const string& )
#if 0 // RFFU
    void Client::onDocumentProtected( const std::string& DocumentId ) {}
#endif
    void Client::getDocument(const std::string& DocumentId) { //! \todo Maybe combine this with getDocuments() function
	http_request Req(methods::GET);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/documents/"+DocumentId));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this,DocumentId](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Get document: " << to_utf8string(Resp.to_string()) << endl;
#endif
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_GET, TYPE_DOCUMENT, DocumentId, string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this](web::json::value Val) {
#ifdef DEBUG_PRINT
			cout << "Received docs (JSON): " << to_utf8string(Val.to_string()) << endl;
#endif
			onDocumentReceived(documentFromJson(Val));
		}), REQ_GET, TYPE_DOCUMENT, DocumentId));
	    }), REQ_GET, TYPE_DOCUMENT, DocumentId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_GET, TYPE_DOCUMENT, DocumentId, string(), -1, Ex.what()); }
    } // getDocument(const string&)
    void Client::onDocumentReceived(const Document& ReceivedDocument) {}
    void Client::checkOperationStatus(const string& OperationId) {
	http_request Req(methods::GET);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/operations/"+OperationId));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this,OperationId](http_response Resp) {
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_GET, TYPE_OPERATION, OperationId, string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this](web::json::value Val) {
#ifdef DEBUG_PRINT
			cout << "Operation (status): " << to_utf8string(Val.to_string()) << endl;
#endif
			onOperationStatusReceived(operationFromJson(Val));
		}), REQ_GET, TYPE_OPERATION, OperationId));
	    }), REQ_GET, TYPE_OPERATION, OperationId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_GET, TYPE_OPERATION, OperationId, string(), -1, Ex.what()); }
    } // checkOperationStatus(const string& )
    void Client::onOperationStatusReceived( const Operation& CurrentOperation ) {}
    void Client::getCopy(const string& DocumentId) {
	http_request Req(methods::GET);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/documents/"+DocumentId+"/content"));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this, DocumentId](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Get copy: " << to_utf8string(Resp.to_string()) << endl;
#endif
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_GET_CONTENT, TYPE_DOCUMENT, DocumentId, string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		string FileName = find_content_file_name(Resp.headers());
#ifdef DEBUG_PRINT
		cout << "Receiving the data." << endl;
#endif
		container_buffer<string> Buf(ios::out | ios::binary);
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.body().read_to_end(Buf).then([this, DocumentId, FileName, Buf](size_t Size) {
			string StrBuf = Buf.collection();
			stringstream Str(StrBuf);
			onDocumentCopyContentReceived(DocumentId, FileName, Str);
		}), REQ_GET_CONTENT, TYPE_DOCUMENT, DocumentId));
	    }), REQ_GET_CONTENT, TYPE_DOCUMENT, DocumentId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_GET_CONTENT, TYPE_DOCUMENT, DocumentId, string(), -1, Ex.what()); }
    } // getCopy(const string&)
    void Client::onDocumentCopyContentReceived(const string& DocumentId, const string& FileName, istream& Content) {}
    void Client::createCopySet(const std::string& Title) {
	http_request Req(methods::POST);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/copysets"));
	Req.headers().set_content_type(U("application/json;charset=UTF-8"));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	web::json::value Val;
	Val[U("title")] = web::json::value(to_string_t(Title));
	Req.set_body(Val);
#ifdef DEBUG_PRINT
	cout << "Create copyset request:" << endl;
	cout << to_utf8string(Req.to_string()) << endl;
#endif
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Create document response: " << to_utf8string(Resp.to_string()) << endl;
#endif
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_CREATE, TYPE_COPYSET, string(), string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this](web::json::value Val) {
#ifdef DEBUG_PRINT
			cout << "New copyset (JSON): " << to_utf8string(Val.to_string()) << endl;
#endif
			onCopySetCreated(copySetFromJson(Val));
		}), REQ_CREATE, TYPE_COPYSET));
	    }), REQ_CREATE, TYPE_COPYSET));
	}
	catch(const exception& Ex) { onRequestFail(REQ_CREATE, TYPE_COPYSET, string(), string(), -1, Ex.what()); }
    } // createCopySet(const std::string&)
    void Client::onCopySetCreated( const CopySet& NewCopySet ) {}
    void Client::addCopyToSet( const string& CopySetId, const string& DocumentId, const string& ForUser ) {
	http_request Req(methods::POST);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/operations"));
	Req.headers().set_content_type(U("application/json;charset=UTF-8"));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	web::json::value Val;
	Val[U("type")] = web::json::value(U("copy"));
	Val[U("document")] = web::json::value(to_string_t(DocumentId));
	Val[U("forUser")] = web::json::value(to_string_t(ForUser));
	Val[U("copySet")] = web::json::value(to_string_t(CopySetId));
	Req.set_body(Val);
#ifdef DEBUG_PRINT
	cout << "Add copy to set request:" << endl;
	cout << to_utf8string(Req.to_string()) << endl;
#endif
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this,DocumentId,CopySetId](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Add copy to set response: " << to_utf8string(Resp.to_string()) << endl;
#endif
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_CREATE, TYPE_COPY, DocumentId, CopySetId, Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this](web::json::value Val) {
#ifdef DEBUG_PRINT
			cout << "Add copy to set (JSON): " << to_utf8string(Val.to_string()) << endl;
#endif
			onOperationStatusReceived(operationFromJson(Val));
		}), REQ_CREATE, TYPE_COPY, DocumentId, CopySetId));
	    }), REQ_CREATE, TYPE_COPY, DocumentId, CopySetId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_CREATE, TYPE_COPY, DocumentId, CopySetId, -1, Ex.what()); }
    } // addCopyToSet( const string&, const string&, const string& )
    void Client::onCopyAdded( const CopySet& Set, const Copy& NewCopy ) {} //!< \todo Implement on autocheck
    void Client::getSetCopies(const std::string& CopySetId) {
	http_request Req(methods::GET);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/copysets/"+CopySetId+"/copies"));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this,CopySetId](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Get set copies: " << to_utf8string(Resp.to_string()) << endl;
#endif
		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_GET_CONTENT, TYPE_COPYSET, CopySetId, string(), Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.extract_json().then([this,CopySetId](web::json::value Val) {
#ifdef DEBUG_PRINT
			cout << "Received copies (JSON): " << to_utf8string(Val.to_string()) << endl;
#endif
			vector<Copy> vCopies;
			if(Val.is_array())
			    for(const web::json::value& MapVal : Val.as_array())
				vCopies.push_back(copyFromJson(MapVal));
			onSetCopiesReceived(CopySetId, vCopies);
		}), REQ_GET_CONTENT, TYPE_COPYSET, CopySetId));
	    }), REQ_GET_CONTENT, TYPE_COPYSET, CopySetId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_GET_CONTENT, TYPE_COPYSET, CopySetId, string(), -1, Ex.what()); }
    } // getSetCopies(const std::string&)
    void Client::onSetCopiesReceived(const std::string& CopySetId, const std::vector<Copy>& Copies) {}

    void Client::getCopyContent(const std::string& CopySetId, const std::string& CopyId) {
	http_request Req(methods::GET);
	Req.set_request_uri(to_string_t(m_sAPIPath+"/copysets/"+CopySetId+"/copies/"+CopyId));
	Req.headers().add(U("Authorization"), to_string_t("Bearer "+m_sOAuth2Token));
	try {
	    m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(m_pPrivate->http().request(Req).then([this, CopySetId, CopyId](http_response Resp) {
#ifdef DEBUG_PRINT
		cout << "Get copy: " << to_utf8string(Resp.to_string()) << endl;
#endif

		if(Resp.status_code() != 200) {
		    onRequestFail(REQ_GET_CONTENT, TYPE_COPY, CopyId, CopySetId, Resp.status_code(), to_utf8string(Resp.to_string()));
		    return;
		}
		string FileName = find_content_file_name(Resp.headers());
#ifdef DEBUG_PRINT
		cout << "Receiving the data." << endl;
#endif
		container_buffer<string> Buf(ios::out | ios::binary);
		m_pPrivate->addTask(new ClientPrivate::GenericRequestInfo(Resp.body().read_to_end(Buf).then([this, CopySetId, CopyId, FileName, Buf](size_t Size) {
			string StrBuf = Buf.collection();
			stringstream Str(StrBuf);
			onCopyContentReceived(CopySetId, CopyId, FileName, Str);
		}), REQ_GET_CONTENT, TYPE_COPY, CopyId, CopySetId));
	    }), REQ_GET_CONTENT, TYPE_COPY, CopyId, CopySetId));
	}
	catch(const exception& Ex) { onRequestFail(REQ_GET_CONTENT, TYPE_COPY, CopyId, CopySetId, -1, Ex.what()); }
    } // getCopyContent(const std::string&, const std::string&)
    void Client::onCopyContentReceived(const string& CopySetId, const string& CopyId, const string& FileName, istream& Content) {}
    void Client::onRequestFail(RequestTypeCode RequestType, ObjectTypeCode ObjectType, const std::string& ObjectID, const std::string& ParentID, int HTTPResponseCode, const std::string& Response) {}
} // ILD
