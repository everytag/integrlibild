#ifndef INTEGRLIBILD_H
#define INTEGRLIBILD_H
#include <string>
#include <vector>
#include <list>
#include <map>

#ifdef WIN32
#ifdef ILDRESTLIB_BUILD_SHARED
#define ILD_CLASS_DEF __declspec(dllexport)
#else
#ifdef ILDRESTLIB_STATIC
#define ILD_CLASS_DEF
#else
#define ILD_CLASS_DEF __declspec(dllimport)
#endif
#endif
#else
#define ILD_CLASS_DEF
#endif

namespace ILD {
    typedef uint64_t timems_t; //!< Время в миллисекундах
    class ILD_CLASS_DEF Credentials {
    public:
	enum GrantType { GRANT_TYPE_PASSWORD };
        Credentials( const std::string& UserName, const std::string& UserPassword );
	// Доступ к полям объекта
	const std::string& userName() const { return m_sUserName; }
	const std::string& userPassword() const { return m_sUserPassword; }
	const std::string& clientName() const { return m_sClientName; }
	const std::string& clientPassword() const { return m_sClientPassword; }
	GrantType grantType() const { return m_eGrantType; }
        // Изменение второстепенных параметров авторизации.
        Credentials& setGrantType(GrantType NewGrantType) { m_eGrantType = NewGrantType; return *this; }
        Credentials& setClientName(const std::string& NewClientName) { m_sClientName = NewClientName; return *this; }
        Credentials& setClientPassword(const std::string& NewClientPassword) { m_sClientPassword = NewClientPassword; return *this; }
    private:
	std::string m_sUserName;
	std::string m_sUserPassword;
	std::string m_sClientName;
	std::string m_sClientPassword;
	GrantType m_eGrantType;
    }; // Credentials

#if 0 // Reserverd for future use (RFFU). Этот, а также следующие исключённые элементы есть в документации по API, но в задание они не входили. Оставлено на случай расширения реализации API.
    //! Права доступа к документу (в API на Postman отсутствуют, поэтому пока не используются)
    class AccessRights {
	bool m_bIsGroup; //!< Признак того, что используемое имя принадлежит группе, а не одному пользователю
	std::string m_sUserName; //!< Имя пользователя или группы
	timems_t m_timeExpire; //!< Время окончания действия разрешения
    }; // AccessRights
#endif

    //! Операция (защита или создание копии), включая статус
    class ILD_CLASS_DEF Operation {
    public:
	Operation(const std::string& Id, const std::string& Type, const std::string& DocumentId, const std::string& UserName, const std::string& State, double m_dProgress = 0,
		  timems_t CreateDate = 0, timems_t StartDate = 0, timems_t EndDate = 0, const std::string& Estimate = std::string(), const std::string& ForUser = std::string(),
		  const std::string& CopySet = std::string(), const std::string& CopyId = std::string(), const std::string& Error = std::string(), const std::string& AccessType = std::string());
	const std::string& id() const { return m_sId; }
	const std::string& documentId() const { return m_sDocumentId; }
	const std::string& userName() const { return m_sUserName; }
	double progress() const { return m_dProgress; }
	timems_t createDate() const { return m_msCreateDate; }
	timems_t startDate() const { return m_msStartDate; }
	timems_t endDate() const { return m_msEndDate; }
	const std::string& state() const { return m_sState; }
	const std::string& type() const { return m_sType; }
	const std::string& estimate() const { return m_sEstimate; }
	const std::string& forUser() const { return m_sForUser; }
	const std::string& copySet() const { return m_sCopySet; }
	const std::string& copyId() const { return m_sCopyId; }
	const std::string& error() const { return m_sError; }
	const std::string& accessType() const { return m_sAccessType; }
    private:
	std::string m_sId;
	std::string m_sDocumentId;
	std::string m_sUserName;
	double m_dProgress; //!< Operation progress 0 to 1;
	timems_t m_msCreateDate;
	timems_t m_msStartDate;
	timems_t m_msEndDate;
	std::string m_sState;
	std::string m_sType;
	std::string m_sEstimate;
	std::string m_sForUser;
	std::string m_sCopySet;
	std::string m_sCopyId;
	std::string m_sError;
	std::string m_sAccessType;
    }; // Operation
    ILD_CLASS_DEF std::string toString(const Operation& Op);

    //! Копия документа
    class ILD_CLASS_DEF Copy {
    public:
    	class ILD_CLASS_DEF Operation {
	public:
	    Operation(int DX = 0, int DY = 0);
	    int dx() const { return m_iDX; }
	    int dy() const { return m_iDY; }
	private:
	    int m_iDX;
	    int m_iDY;
	}; // Operation
	class ILD_CLASS_DEF Map {
	public:
	    Map(int Number = 0, const std::map<int,Operation>& Operations = std::map<int,Operation>());
	    int number() const { return m_iNumber; }
	    void setOperation(int Number, const Operation& NewOperation);
	    const std::map<int,Operation> operations() const { return m_mOperations; }
	private:
	    int m_iNumber;
	    std::map<int,Operation> m_mOperations;
	}; // Map
	Copy(const std::string& Id, const std::string& CopySetId, const std::string& DocumentId, const std::string& FileId, const std::string& UserName, const std::string& ForUser, 
	     timems_t m_msCreateDate, timems_t m_msDeleteDate = 0, std::string m_sAccess = std::string(), const std::vector<Map>& Maps = std::vector<Map>());
	const std::string& id() const { return m_sId; }
	const std::string& copySetId() const { return m_sCopySetId; }
	const std::string& documentId() const { return m_sDocumentId; }
	const std::string& fileId() const { return m_sFileId; }
	timems_t createDate() const { return m_msCreateDate; }
	timems_t deleteDate() const { return m_msDeleteDate; }
	const std::string& userName() const { return m_sUserName; }
	const std::string& forUser() const { return m_sForUser; }
	const std::string& access() const { return m_sAccess; }
	const std::vector<Map>& maps() const { return m_vMaps; }
    private:
	std::string m_sId;
	std::string m_sCopySetId;
	std::string m_sDocumentId;
	std::string m_sFileId;
	std::string m_sUserName;
	std::string m_sForUser;
	timems_t m_msCreateDate;
	timems_t m_msDeleteDate;
	std::string m_sAccess;
	std::vector<Map> m_vMaps;
    }; // Copy
    ILD_CLASS_DEF std::string toString(const Copy& TheCopy);
   
    //! Набор копий
    class ILD_CLASS_DEF CopySet {
    public:
	CopySet(const std::string& Id, const std::string& Title, timems_t CreateDate = 0, timems_t HandedDate = 0);
	const std::string& id() const { return m_sId; }
	const std::string& title() const { return m_sTitle; }
	timems_t createDate() const { return m_msCreateDate; }
	timems_t handedDate() const { return m_msHandedDate; }
    private:
	std::string m_sId;
	std::string m_sTitle;
	timems_t m_msCreateDate;
	timems_t m_msHandedDate;
    }; // CopySet
    ILD_CLASS_DEF std::string toString(const CopySet& Set);
	
    //! Документ или папка документов
    class ILD_CLASS_DEF Document {
    public:
	enum StateCode { STATE_UNPROTECTED = 0, STATE_PROTECTED };
	//! Атрибут документа
	class ILD_CLASS_DEF Attribute {
	public:
	    Attribute( const std::string& Name, const std::string& Value, const std::string& Type = "string" );
	    const std::string& name() const { return m_sName; }
	    const std::string& type() const { return m_sType; }
	    const std::string& value() const { return m_sValue; }
	private:
	    std::string m_sName; //!< Название атрибута
	    std::string m_sType; //!< Тип атрибута - как строка, для поддержки неизвестных типов.
	    std::string m_sValue; //!< Значение - как строка (в JSONе это всё равно так). Преобразование возложено на пользователя, в примере все атрибуты были строковыми.
	}; // Attribute
#if 0 // RFFU
	//! Страница документа
	class ILD_CLASS_DEF Page {
	public:
	    Page( const std::string& Id, const std::string& DocumentId, int Number, const std::string& ImageId = std::string(), const std::string& MapId = std::string(),
		  const std::string& m_sInvestigationId = std::string() );
	private:
	    std::string m_sId; //!< Идентификатор страницы
	    std::string m_sDocumentId; //!< Идентификатор документа, к которому относится данная страница
	    int m_iNumber; //!< Номер страницы в документе
	    std::string m_sImageId; //!< Идентификатор изображения страницы
	    std::string m_sMapId; //!< Идентификатор карты (блоков?) странцы
	    std::string m_sInvestigationId; //!< Идентификатор расследования, открытого по этой странице
	}; // Page
#endif

	Document(const std::string& Id, const std::string& Title, const std::string& CreatedBy = std::string(), const std::string& ModifiedBy = std::string(), timems_t Timecreated = 0,
		 timems_t TimeModified = 0, const std::string& ContentId = std::string(), StateCode State = STATE_UNPROTECTED, const std::vector<Attribute>& Attributes = std::vector<Attribute>());
	Document(const std::string& Title, const std::vector<Attribute>& Attributes = std::vector<Attribute>());
	const std::string& id() const { return m_sId; }
	const std::string& title() const { return m_sTitle; }
	const std::string& createdBy() const { return m_sCreatedBy; }
	timems_t createdTime() const { return m_timeCreated; }
	const std::string& modifiedBy() const { return m_sModifiedBy; }
	timems_t modifiedTime() const { return m_timeModified; }
	StateCode state() const { return m_eState; }
	bool isProtected() const { return m_eState == STATE_PROTECTED; } 
	const std::vector<Attribute>& attributes() const { return m_vAttributes; } //!< Возвращает все атрибуты
	const Attribute& attribute(const std::string& Name) const; //!< Возвращает атрибут с указанным именем
	void setAttribute(const Attribute& NewAttribute); //!< Изменение или добавление одного атрибута
	void setAttributes(const std::vector<Attribute>& NewAttributes); //!< Заменяет все атрибуты (быстрее)
    private:
	std::string m_sId; //!< Идентификатор документа (папки?)
#if 0 // RFFU
	std::string m_sFolderId; //!< Идентификатор папки, к которой относится данный документ
#endif
	std::string m_sTitle; //!< Заголовок документа
	std::string m_sCreatedBy; //!< Имя пользователя, создавшего документ
	std::string m_sModifiedBy; //!< Имя пользователя, последним изменившего документ
	std::string m_sContentId; //!< Идентификатор содержимого документа
	timems_t m_timeCreated; //!< Время создания в мс
	timems_t m_timeModified; //!< Время изменения в мс
	// Следующие атрибуты относятся только к документу
	StateCode m_eState;  //!< Защищён ли документ
	std::vector<Attribute> m_vAttributes; //!< Атрибуты документа
#if 0 // RFFU
	std::vector<Page> m_vPages; //!< Страницы документа
#endif
    }; // Document
    
    //! Клиент ILD, обеспечивает подключение к серверу и выполнение всех операций, заложенных в 
    class ILD_CLASS_DEF Client {
    public:
	enum RequestTypeCode { //!< Тип запроса - для определения запрошеннй операции с объектом, которая закончилась неудачей.
			      REQ_UNKNOWN,	//!< Операция не определена (на всякий случай)
			      REQ_GET,		//!< Получение объекта (информации о нём), также проверка статуса операции
			      REQ_CREATE,	//!< Содздание объекта, также добавление копии в набор
			      REQ_UPLOAD,	//!< Загрузка содержимого
			      REQ_PROTECT,	//!< Защита документа
			      REQ_GET_CONTENT	//!< Получение содержимого объекта (документа, копии, списка копий из набора или списка документов).
	}; // RequestTypeCode
	enum ObjectTypeCode { //!< Тип объекта. В основном для определения того, что за объект не найден в запросе
			     TYPE_UNKNOWN,	//!< Тип объекта неизвестен
			     TYPE_DOCUMENT,	//!< Документ
			     TYPE_FOLDER,	// Папка (используется для получения списка документов)
			     TYPE_OPERATION,	//!< Операция
			     TYPE_COPY,		//!< Копия
			     TYPE_COPYSET	//!< Набор копий
	}; // ObjectTypeCode
	static std::string http_code_description(int Code);
	Client( const std::string& ServiceURI = "http://base.everytag.ru", int Port = 8080, int Version = 1 );
	virtual ~Client();
	//! \brief Проверка окончания всех запросов.
	//! В отличие от функции waitRequests, эта не ждёт окончания выполняющихся запросов, а только проверяет завершённые. Это необходимо, так как используемый фреймворк не сообщает о возникшей сетевой
	//! ошибке, например, что сервер не найден, пока его об этом не спросят.
	//! \return Количество активных (ещё не завершённых) запросов.
	size_t checkRequests();
	//! \brief Ожидание окончания всех запросов.
	//! Этот метод должен быть вызван перед завершением программы, чтобы дождаться окончания всех асинхронных запросов.
	//! Следует иметь в виду, что, если его поместить в деструкторе, перегруженные в наследниках методы не будут вызваны.
	void waitRequests();
	//! \defgroup clientrequests Запросы к сервису ILD
	//! @{ Следующие методы запускают запросы к сервису. Результаты приходят в соответствующие виртуальные callback'и, которые для этого необходимо переопределить.
	
	//! Получение OAuth2 токена. Без успешного выполнения этой операции никакие другие запросы невозможны.
	void authorize(const Credentials& ClientCredentials);
	bool isAuthorized();
	void unAuthorize();
	//! Получить список всех документов.
	void getDocuments();
	//! Создать документ (объект)
	void createDocument(const Document& NewDocument);
        //! \brief Загрузка содержимого (оригинала) документа
        //! Данная операция только загружает документ из файла или из предоставленного потока (в этом случае FileName несёт исключительно справочную информацию. Защита документа начинается после вызова
        //! protectDocument.
	void uploadOriginal(const std::string& DocumentId, const std::string& FileName, std::istream& Content);
	void uploadOriginal(const std::string& DocumentId, const std::string& FileName); //!< Перегруженная функция, которая открывает указанный файл.
	//! \brief Запуск процесса защиты документа
        //! Этот вызов запускает процесс защиты документа. Состояние операции нужно проверять следующим вызовом.
        //! В случае продолжения работы над библиотекой будет реализована автоматическая проверка с заданным интервалом, по окончании защиты - callback.
	void protectDocument(const std::string& DocumentId);
	//! Получение информации о документе (включая статус)
	void getDocument(const std::string& DocumentId);
	//! Проверка состояния текущей операции.
	void checkOperationStatus(const std::string& OperationId);
	//! Запуск создания копии. Процесс проверяется предыдущей функцией.
	void getCopy(const std::string& DocumentId);
	//! Создание набора копий
	void createCopySet(const std::string& Title);
	//! Добавление копии в набор
	void addCopyToSet(const std::string& CopySetId, const std::string& DocumentId, const std::string& ForUser);
	//! Получение списка копий, входящих в набор
	void getSetCopies(const std::string& CopySetId );
	//! Получение содержимого копии из набора
	void getCopyContent(const std::string& CopySetId, const std::string& CopyId);
	//! @}
    protected:
	//! \defgroup clientcallbacks Методы для переопределения в клиенте
	//! @{
	//
	//! Авторизация выполнена успешно (токен OAuth2 уже получен и установлен в соответствующее поле данных)
	virtual void onAuthorizationSuccess();
	//! Запрошенные документы получены
	virtual void onDocumentsReceived(const std::vector<Document>& Documents);
	//! Документ создан
	virtual void onDocumentCreated(const Document& NewDocument);
	//! Содержимое документа успешно загружено
	virtual void onDocumentContentUploaded( const std::string& DocumentId );
#if 0 // RFFU Для автоматической проверки, если она будет реализована.
	//! Документ защищён
	virtual void onDocumentProtected( const std::string& DocumentId );
#endif
	//! Получен (статус?) документа
	virtual void onDocumentReceived( const Document& ReceivedDocument );
	//! Запрошенный статус операции
	virtual void onOperationStatusReceived( const Operation& CurrentOperation );
	//! Копия защищённого документа готова
	virtual void onDocumentCopyContentReceived(const std::string& DocumentId, const std::string& FileName, std::istream& Content);
	//! Набор копий успешно создан
	virtual void onCopySetCreated(const CopySet& NewCopySet);
	//! Копия добавлена в набор
	virtual void onCopyAdded(const CopySet& Set, const Copy& NewCopy);
	//! Получены копии, входящие в набор
	virtual void onSetCopiesReceived(const std::string& CopySetId, const std::vector<Copy>& Copies);
	//! Получено содержимое копии из набора
	virtual void onCopyContentReceived(const std::string& CopySetId, const std::string& CopyId, const std::string& FileName, std::istream& Content);
	//! \defgroup clienterrorbacks Методы для переопределения в случае нештатных ситуаций
	//! \note Использование исключений затруднено асинхронностью работы фреймворка (C++ REST SDK), из-за которой они порой возникают в неожиданных местах.
	//! @{
	//
	//! Ошибка авторизации
	//! \param ClientCredentials - параметры авторизации, которые вызвали ошибку
	//! \param HTTPResponseCode - код ответа HTTP-сервера, <0, если ответ не получен (например, нет сети)
	//! \param Response - полный ответ сервера или информация о причине сбоя, если он не был получен.
	virtual void onAuthorizationFail(const Credentials& ClientCredentials, int HTTPResponseCode, const std::string& Response);
	//! Ошибка запроса к серверу
	//! \param ReqestType - тип запроса
	//! \param ObjectType - тип объекта, с которым связан запрос
	//! \param ObjectID - идентификатор объекта, с которым связан запрос
	//! \param ParentID - идентификатор родительского объекта (например, набор копий), если есть, иначе - пустая строка
	//! \param HTTPResponseCode - код ответа HTTP-сервера, <0, если ответ не получен (например, нет сети)
	//! \param Response - полный ответ сервера или информация о причине сбоя, если он не был получен.
	virtual void onRequestFail(RequestTypeCode RequestType, ObjectTypeCode ObjectType, const std::string& ObjectID, const std::string& ParentID, int HTTPResponseCode, const std::string& Response);
	//! @}
	//! @}
    private:
	std::string m_sServiceURI; //!< Общий URI сервиса ILD без указания порта и пути (только схема и имя хоста.
	std::string m_sAPIPath; //!< Префикс пути для запросов API - "/api/v{номер версии}".
	int m_iPort; //!< Порт, на котором принимает запросы сервис ILD.
	int m_iVersion; //!< Версия REST API ILD.
	std::string m_sUserName;
	std::string m_sOAuth2Token;
	class ClientPrivate;
	ClientPrivate* m_pPrivate;
    }; // Client
} // ILD
#endif // INTEGRLIBILD_H
