rem Update SOURCES_PATH and VCPKG_PATH to point to your lib sources and vcpkg root folder. Out 
set SOURCES_PATH=..\integrlibild
set VCPKG_PATH=..\..\vcpkg
echo Building x64 Debug...
mkdir x64debug
cd x64debug
cmake -DCMAKE_TOOLCHAIN_FILE=%VCPKG_PATH%\scripts\buildsystems\vcpkg.cmake -A x64 -DCMAKE_BUILD_TYPE=Debug %SOURCES_PATH%
cmake --build . --config Debug
cd ..
echo Building x64 Release...
mkdir x64release
cd x64release
cmake -DCMAKE_TOOLCHAIN_FILE=%VCPKG_PATH%\scripts\buildsystems\vcpkg.cmake -A x64 -DCMAKE_BUILD_TYPE=Release %SOURCES_PATH%
cmake --build . --config Release
cd ..
echo Building Win32 Debug...
mkdir w32debug
cd w32debug
cmake -DCMAKE_TOOLCHAIN_FILE=%VCPKG_PATH%\scripts\buildsystems\vcpkg.cmake -A Win32 -DCMAKE_BUILD_TYPE=Debug %SOURCES_PATH%
cmake --build . --config Debug
cd ..
echo Building Win32 Release...
mkdir w32release
cd w32release
cmake -DCMAKE_TOOLCHAIN_FILE=%VCPKG_PATH%\scripts\buildsystems\vcpkg.cmake -A Win32 -DCMAKE_BUILD_TYPE=Release %SOURCES_PATH%
cmake --build . --config Release
cd ..
echo Copying files...
mkdir libxxx
mkdir libxxx\include
copy /B %SOURCES_PATH%\include\*.h libxxx\include
mkdir libxxx\bin
mkdir libxxx\bin\debug
copy /B w32debug\lib\debug\Debug\*.dll libxxx\bin\debug
copy /B w32debug\lib\debug\Debug\*.pdb libxxx\bin\debug
mkdir libxxx\bin\release
copy /B w32release\lib\release\Release\*.dll libxxx\bin\release
mkdir libxxx\bin64
mkdir libxxx\bin64\debug
copy /B x64debug\lib64\debug\Debug\*.dll libxxx\bin64\debug
copy /B x64debug\lib64\debug\Debug\*.pdb libxxx\bin64\debug
mkdir libxxx\bin64\release
copy /B x64release\lib64\release\Release\*.dll libxxx\bin64\release
mkdir libxxx\lib
mkdir libxxx\lib\debug
copy /B w32debug\lib\debug\Debug\*.lib libxxx\lib\debug
copy /B w32debug\lib\debug\Debug\*.exp libxxx\lib\debug
copy /B w32debug\lib\debug\Debug\*.ilk libxxx\lib\debug
mkdir libxxx\lib\release
copy /B w32release\lib\release\Release\*.lib libxxx\lib\release
copy /B w32release\lib\release\Release\*.exp libxxx\lib\release
mkdir libxxx\lib64
mkdir libxxx\lib64\debug
copy /B x64debug\lib64\debug\Debug\*.lib libxxx\lib64\debug
copy /B x64debug\lib64\debug\Debug\*.exp libxxx\lib64\debug
copy /B x64debug\lib64\debug\Debug\*.ilk libxxx\lib64\debug
mkdir libxxx\lib64\release
copy /B x64release\lib64\release\Release\*.lib libxxx\lib64\release
copy /B x64release\lib64\release\Release\*.exp libxxx\lib64\release
mkdir libxxx\test
mkdir libxxx\test\debug
copy /B w32debug\test\debug\Debug\*.exe libxxx\test\debug
copy /B w32debug\test\debug\Debug\*.pdb libxxx\test\debug
mkdir libxxx\test\release
copy /B w32release\test\release\Release\*.exe libxxx\test\release
mkdir libxxx\test64
mkdir libxxx\test64\debug
copy /B x64debug\test64\debug\Debug\*.exe libxxx\test64\debug
copy /B x64debug\test64\debug\Debug\*.pdb libxxx\test64\debug
mkdir libxxx\test64\release
copy /B x64release\test64\release\Release\*.exe libxxx\test64\release
echo Done.
