# Сборка в среде Ubuntu 18.04 (актуальная LTS):

Создаём каталог для сборки, например, build и переходим в него (`cd build`).

## Для одной конфигурации для использования в той же версии ОС.
Устанавливаем необходимые пакеты:
`sudo apt-get install libcpprest-dev cmake libboot-filesystem-dev`
`cmake -DCMAKE_BUILD_TYPE=Debug <путь к исходникам>`
Здесь "путь к исходникам" указывает на корневой каталог исходников библиотеки, например: "../integrlibild/", в случае релизной сборки нужно указать `-DCMAKE_BUILD_TYPE=Release`.
`make`
Для запуска собранного приложения на другом компьютере с той же ОС необходимо будет установить пакеты:
`sudo apt-get install libcpprest2.10 libboost-filesystem1.65.1`

## Для нескольких конфигураций: 32/64 bit x debug/release.
Устанавливаем необходимые пакеты:
`sudo apt-get install g++-6-multilib g++-6-multilib-i686-linux-gnu libcpprest-dev cmake libboot-filesystem-dev`
Собираем отладочную конфигурацию:
`cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=/usr/bin/x86_64-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/x86_64-linux-gnu-g++-6 <путь к исходникам>`
Здесь "путь к исходникам" указывает на корневой каталог исходников библиотеки, например: "../integrlibild/".
`make`
Копируем lib64/debug/libintegrlibild.so и test64/debug/resttest в соответствующие выходные каталоги.
Очищаем каталог сборки:
`rm -rf CMakeCache.txt CMakeFiles cmake_install.cmake Makefile`
Или просто `rm -rf *` (предварительно дважды убедившись, что находимся именно в нём, и не положили туда ничего нужного):
Собарием релизную конфигурацию:
`cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=/usr/bin/x86_64-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/x86_64-linux-gnu-g++-6 <путь к исходникам>`
`make`
Копируем lib64/release/libintegrlibild.so и test64/release/resttest в соответствующие выходные каталоги.
Очищаем каталог сборки (см. выше).

После этого устанавливаем пакеты для 32-битной архитектуры, при этом удаляются пакеты и ломается часть зависимостей для 64-битной.
`sudo apt-get install libicu-dev:i386 libboost-regex1.65-dev:i386  libboost-regex-dev:i386 libcpprest-dev:i386`
Собираем 32-битную отладочную конфигурацию:
`cmake -DCMAKE_BUILD_TYPE=Debug -DILD_BITS=32 -DCMAKE_C_COMPILER=/usr/bin/i686-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/i686-linux-gnu-g++-6 ../integrlibild/`
`make`
Копируем lib/debug/libintegrlibild.so и test/debug/resttest в соответствующие выходные каталоги.
Очищаем каталог сборки (см. выше).
Собираем 32-битную релизную конфигурацию:
`cmake -DCMAKE_BUILD_TYPE=Release -DILD_BITS=32 -DCMAKE_C_COMPILER=/usr/bin/i686-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/i686-linux-gnu-g++-6 ../integrlibild/`
`make`
Копируем lib/release/libintegrlibild.so и test/release/resttest в соответствующие выходные каталоги.
Очищаем каталог сборки (см. выше) или удаляем его за ненадобностью.

Копируем в выходные каталоги bin и bin64 библиотеки, которые выдают команды `ldd lib/debug/libintegrlibild.so` и `ldd lib64/debug/libintegrlibild.so` соответственно.

# Сборка в среде MS Windows с MS Visual Studio 2015.

Устанавливаем Git (можно штатный VS) и CMake.

Далее в командной строке Visual C++:
Создаём папку для сборки **без русских (и прочих нелатинских) букв и пробелов**, желательно поближе к корню диска, например, C:\build, переходим в неё.
Клонируем в неё репозиторий vcpkg: `git clone https://github.com/Microsoft/vcpkg.git`
Переходим в каталог vcpkg и собираем: `bootstrap-vcpkg.bat`
Выполняем `vcpkg integrate install` (подробнее: https://github.com/Microsoft/vcpkg)

Устанавливаем cpprestsdk и boost-filesystem: `vcpkg install cpprestsdk cpprestsdk:x64-windows boost-filesystem boost-filesystem:x64-windows` (процесс достаточно длительный, требуется несколько гигабайт на диске, подробнее: https://github.com/microsoft/cpprestsdk).

Для сборки нужно создать специальную папку, и каждую конфигурацию платформы и типа (отладка/релиз) собирать отдельно:
`cmake -DCMAKE_TOOLCHAIN_FILE=<путь к папке vcpkg>\scripts\buildsystems\vcpkg.cmake -A <платформа> -DCMAKE_BUILD_TYPE=<тип сборки> <путь к папке с исходниками библиотеки>`
где "путь к папке vcpkg" - полный путь к папке, в которой был собран vcpkg, должен содержать подкаталог scripts\buildsystems с файлом vcpkg.cmake;
"платформа" - x64 или Win32, как это называется в MS Visual Studio;
"тип сборки" - Release или Debug;
"путь к папке с исходниками библиотеки" - абсолютный или относительный путь к исходникам данного проекта.
После настройки каталога сборки, выполняется команда `cmake --build . --config <тип сборки>`, где "тип сборки" должен совпадать с указанным ранее, или полученный проект можно открыть в среде Visual Studio, но следует иметь в виду, что комбинации платформы и типа сборки, кроме указанной в первой команде cmake могут работать неверно.
После выполнения сборки полученные файлы нужно скопировать в соответствующие выходные папки. Для сборки другой конфигурации папку для сборки - очистить от файлов, кроме папок lib, lib64, test и test64 или удалить и пересоздать. Или использовать отдельные папки для каждой конфигурации, как в приложенном примере командного файла для сборки по MS Windows `build_vs.cmd`.