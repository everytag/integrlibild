#!/bin/sh
SOURCE_PATH=../../../integrlibild
date
case "$1" in
    64)
	echo Building 64-bit debug version
	(mkdir -p x64/debug && cd x64/debug &&
	     cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=/usr/bin/x86_64-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/x86_64-linux-gnu-g++-6 "$SOURCE_PATH" && make && cd ../..) || exit 1
	echo Building 64-bit release version
	(mkdir -p x64/release && cd x64/release &&
	     cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=/usr/bin/x86_64-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/x86_64-linux-gnu-g++-6 "$SOURCE_PATH" && make && cd ../..) || exit 1
	;;
    32)
	echo Building 32-bit debug version
	(mkdir -p x32/debug && cd x32/debug &&
	     cmake -DCMAKE_BUILD_TYPE=Debug -DILD_BITS=32 -DCMAKE_C_COMPILER=/usr/bin/i686-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/i686-linux-gnu-g++-6 "$SOURCE_PATH" && make && cd ../..) || exit 1
	echo Building 32-bit release version
	(mkdir -p x32/release && cd x32/release &&
	     cmake -DCMAKE_BUILD_TYPE=Release -DILD_BITS=32 -DCMAKE_C_COMPILER=/usr/bin/i686-linux-gnu-gcc-6 -DCMAKE_CXX_COMPILER=/usr/bin/i686-linux-gnu-g++-6 "$SOURCE_PATH" &&
	     make && cd ../..) || exit 1
	;;
    pack)
	(mkdir -p libxxx/include && cd libxxx/include && cp -vp "$SOURCE_PATH"/include/integrlibild.h . && cd ../..) || exit 2
	for BITS in 64 32; do
	    if [ "$BITS" -eq "64" ]; then SUFFIX="64"; else SUFFIX=""; fi
	    for BUILD in debug release; do 
		(mkdir -p libxxx/lib$SUFFIX/$BUILD && cp -vp x$BITS/$BUILD/lib$SUFFIX/$BUILD/libintegrlibild.so libxxx/lib$SUFFIX/$BUILD/ &&
		     mkdir -p libxxx/test$SUFFIX/$BUILD && cp -vp x$BITS/$BUILD/test$SUFFIX/$BUILD/resttest libxxx/test$SUFFIX/$BUILD/ ) || exit 2
	    done
	    (mkdir -p libxxx/bin$SUFFIX && cp -vp $(ldd libxxx/lib$SUFFIX/debug/libintegrlibild.so | sed -e 's%.*[[:space:]]/%/%' -e 's%[[:space:]].*%%') libxxx/bin$SUFFIX/) || exit 2
	done
	(mkdir -p linux && mv libxxx -v linux/) || exit 2
	;;
    *) echo Usage: $(basename "$0") 'with a single parameter: "64" to build 64-bit binaries, "32" - 32-bit binaries or "pack" to prepare the binary package dir in libxxx' ;;
esac

exit 0
